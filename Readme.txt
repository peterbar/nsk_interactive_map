Interactive overview map of Northern Saskatchewan (Census Division 18, SK).
By Petr Baranovskiy for the University of Saskatchewan.


Data sources: 

Natural Resources Canada. 2018. "Indigenous Mining Agreements Lands and Minerals Sector." Accessed July 5, 2019. http://atlas.gc.ca/imaema/en/.

Natural Resources Canada. 2018. "Aboriginal Lands of Canada Legislative Boundaries." Accessed July 5, 2019. https://open.canada.ca/data/en/dataset/522b07b9-78e2-4819-b736-ad9208eb1067.

Statistics Canada. 2016. "2016 Census - Boundary files." Accessed July 5, 2019. https://www12.statcan.gc.ca/census-recensement/2011/geo/bound-limit/bound-limit-2016-eng.cfm.

Statistics Canada. 2016. "2016 Census Data. Saskatchewan." Accessed July 5, 2019. https://www12.statcan.gc.ca/census-recensement/2016/dp-pd/prof/details/download-telecharger/comp/GetFile.cfm?Lang=E&FILETYPE=CSV&GEONO=068.

Statistics Canada. n.d. "Canadian Geographical Names - CGN - GeoBase Series." Accessed July 5, 2019. https://open.canada.ca/data/en/dataset/e27c6eba-3c5d-4051-9db2-082dc6411c2c.


R packages:

Bhaskar Karambelkar and Barret Schloerke. 2018. "leaflet.extras: Extra Functionality for 'leaflet' Package." R package version 1.0.0. https://CRAN.R-project.org/package=leaflet.extras.

Hadley Wickham. 2017. "tidyverse: Easily Install and Load the 'Tidyverse'." R package version 1.2.1. https://CRAN.R-project.org/package=tidyverse.

Joe Cheng, Bhaskar Karambelkar and Yihui Xie. 2018. "leaflet: Create Interactive Web Maps with the JavaScript 'Leaflet' Library." R package version 2.0.2. https://CRAN.R-project.org/package=leaflet.

Pebesma, E. 2018. "Simple Features for R: Standardized Support for Spatial Vector Data." The R Journal 10 (1), 439-446. https://doi.org/10.32614/RJ-2018-009.

Pebesma E, Mailund T, Hiebert J. 2016. “Measurement Units in R.” R Journal, 8(2), 486-494. doi: 10.32614/RJ-2016-061. https://doi.org/10.32614/RJ-2016-061.

Ramnath Vaidyanathan, Yihui Xie, JJ Allaire, Joe Cheng and Kenton Russell. 2018. "htmlwidgets: HTML Widgets for R." R package version 1.3. https://CRAN.R-project.org/package=htmlwidgets.

von Bergmann, J., Dmitry Shkolnik, and Aaron Jacobs. 2018. "cancensus: Canadian Census Data and Geography from the 'CensusMapper' API." v0.1.8.

